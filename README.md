# pygencode

#### 介绍
python代码生成器


#### 配套文章

 [代码生成器原理及实现-Python3版](https://juejin.im/post/5eb0353df265da7b9f07adad) 


#### 安装教程
1. 拉取代码
``` shell
git clone https://gitee.com/mldong/pygencode.git
```
2. 进入目录
``` shell
cd pygencode
```
3. 创建venv
``` shell
python3 -m venv ./venv
```
4. venv生效
``` shell
source ./venv/bin/activate
```
5. 安装依赖
``` shell
pip install -r requirements.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com
```

#### 使用说明
``` shell
python main.py -h
usage: main.py [-h] [-c CONFIG] [-d DATATYPE] [-t TABLENAME] [-m MODE]

代码生成器

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        配置文件
  -d DATATYPE, --dataType DATATYPE
                        数据类型转换配置文件
  -t TABLENAME, --tableName TABLENAME
                        要代码生成的表
  -m MODE, --mode MODE  1->生成代码,2->查看元数据

```
默认配置文件
config/app.yml


