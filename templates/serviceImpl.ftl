package {{basePackage}}.{{moduleName}}.service.impl;

import org.springframework.stereotype.Service;

import {{basePackage}}.{{moduleName}}.domain.{{table.className}};
import {{basePackage}}.{{moduleName}}.repository.mapper.{{table.className}}Mapper;
import {{basePackage}}.{{moduleName}}.service.{{table.className}}Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
* {{table.remark}}Service业务层
*/
@Service
public class{{table.className}}ServiceImpl extends ServiceImpl<{{table.className}}Mapper, {{table.className}}> implements {{table.className}}Service {
}
