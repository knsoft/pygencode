package {{basePackage}}.{{moduleName}}.portal.msapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import {{basePackage}}.{{moduleName}}.domain.{{table.className}};
import {{basePackage}}.{{moduleName}}.msapi.{{table.className}}FeignService;
import {{basePackage}}.{{moduleName}}.service.{{table.className}}Service;
import {{basePackage}}.framework.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.AllArgsConstructor;

/**
 * {{table.remark}}
 */
@RestController
@AllArgsConstructor
public class {{table.className}}FeignServiceImpl implements {{table.className}}FeignService {
	
    private final {{table.className}}Service {{table.tableCameName}}Service;
    
    private QueryWrapper<{{table.className}}> getQueryWrapper({{table.className}} {{table.tableCameName}}) {
    	QueryWrapper<{{table.className}}> queryWrapper = new QueryWrapper<{{table.className}}>();
    	if ({{table.tableCameName}} == null) {
    		return queryWrapper;
    	}
        return queryWrapper;
    }

    @Override
    public Page<{{table.className}}> list({{table.className}} {{table.tableCameName}}, long current, long size) {
    	Page<{{table.className}}> page = new Page<{{table.className}}>(current, size);
    	Page<{{table.className}}> {{table.tableCameName}}Page = {{table.tableCameName}}Service.page(page, getQueryWrapper({{table.tableCameName}}));
		return {{table.tableCameName}}Page;
    }

	@Override
	public {{table.className}} getById(Integer id) {
		return {{table.tableCameName}}Service.getById(id);
	}

	@Override
	public boolean save({{table.className}} {{table.tableCameName}}) {
		return {{table.tableCameName}}Service.save({{table.tableCameName}});
	}

	@Override
	public boolean update({{table.className}} {{table.tableCameName}}) {
		return {{table.tableCameName}}Service.updateById({{table.tableCameName}});
	}

	@Override
	public boolean remove(Integer[] id) {
		return {{table.tableCameName}}Service.removeByIds(Arrays.asList(id));
	}

	@Override
	public List<{{table.className}}> list({{table.className}} {{table.tableCameName}}) {
		List<{{table.className}}> list = {{table.tableCameName}}Service.list(getQueryWrapper({{table.tableCameName}}));
		return list;
	}
}