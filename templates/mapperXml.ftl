<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="{{basePackage}}.{{moduleName}}.repository.mapper.{{table.className}}Mapper">
    <resultMap id="BaseResultMap" type="{{basePackage}}.{{moduleName}}.domain.{{table.className}}">
        {% for column in table.columns %}{%if column.primaryKey %}
            <id column="{{column.columnName}}" property="{{column.javaProperty}}" />{% else %}
            <result column="{{column.columnName}}" property="{{column.javaProperty}}" />{% endif %}
        {% endfor %}
    </resultMap>
</mapper>